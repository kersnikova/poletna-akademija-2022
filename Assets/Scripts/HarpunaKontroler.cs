using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarpunaKontroler : MonoBehaviour
{
    public float hitrost = 2f;
    Vector3 currentEulerAngles;
    Quaternion currentRotation;
    bool zacelStrel = false;
    bool naCilju = false;
    public float hitrostHarpune = 5f;
    public float hitrostHarpuneNazaj = 3f;
    Vector3 cilj;
    Vector3 koncniCilj;
    Vector3 izhodisce;
    public LineRenderer vrv;
    public bool ujel = false;
    public int nagrada;
    public GameObject plen;

    void Start()
    {
        izhodisce = transform.position;
    }

    
    void Update()
    {
        Ray r = new Ray(transform.position, transform.TransformDirection(Vector3.up));
        cilj = r.GetPoint(5);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out RaycastHit hitinfo, 20f))
        {
            Debug.Log("hit");
         Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * hitinfo.distance, Color.red);

        }
        else
        {
            Debug.Log("miss");
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 5f, Color.green);
        }
        if ((Input.GetKeyDown(KeyCode.C)) && !zacelStrel && !naCilju)
        {
            zacelStrel = true;
            koncniCilj = cilj;
            vrv.enabled = true;
                    }
        if (zacelStrel)
        {
            if (transform.position == koncniCilj)
            {
                naCilju = true;
                zacelStrel = false;
                Debug.Log("finito");
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, koncniCilj, hitrostHarpune * Time.deltaTime);
                //transform.Translate(Vector3.up*Time.deltaTime*hitrostHarpune,Space.Self);
            }
        }
        if (naCilju)
        {
            if (transform.position == izhodisce)
            {
                naCilju = false;
                vrv.enabled = false;
                ujel = false;
                nagrada += 10;
                Destroy(plen);

            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, izhodisce, hitrostHarpuneNazaj * Time.deltaTime);
            }
        }

        vrv.SetPosition(0, izhodisce);
        vrv.SetPosition(1, transform.position);


        //premikanje
        Vector3 rotacija = new Vector3(0, 0, Input.GetAxis("Horizontal"));

        currentEulerAngles += rotacija * Time.deltaTime * hitrost * 100;
        currentRotation.eulerAngles = currentEulerAngles;
        transform.rotation = currentRotation;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "smet")
        {

            plen = other.gameObject;
            ujel = true;
            Debug.Log("dotaknil se smeti");
            other.transform.parent = gameObject.transform;
            Rigidbody others = other.GetComponent<Rigidbody>();
            others.isKinematic = true;

        }
    }
}
