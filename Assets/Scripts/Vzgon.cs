using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vzgon : MonoBehaviour
{

    public float underWaterDrag = 3;
    public float underWaterAngularDrag = 1;
    public float airDrag = 0;
    public float airAngularDrag = 0.5f;

    Rigidbody rigid;
    bool underWater;
    public float floatingPower = 15f;
    public float waterHeight = 0f;

    public float casOdSpawna;
    public float casSpawna;


    void Start()
    {
        transform.rotation = new Quaternion(Random.Range(1, 180), Random.Range(1, 180), Random.Range(1, 180), Random.Range(1, 180));

        rigid = GetComponent<Rigidbody>();
        casSpawna = Time.time;
    }


    void FixedUpdate()
    {
        casOdSpawna = Time.time - casSpawna;

        waterHeight += (Mathf.Sin(Time.time)) / 20;
        float diff = transform.position.y - waterHeight;

        if (casOdSpawna > 10)
        {

            waterHeight -= Time.deltaTime / 10;

        }
        if (diff < 0)
        {
            rigid.AddForceAtPosition(Vector3.up * floatingPower * Mathf.Abs(diff), transform.position, ForceMode.Force);
            if (!underWater)
            {
                underWater = true;
                SwitchState(true);
            }
        }
        else if (underWater)
        {
            underWater = false;
            SwitchState(false);
        }
    }

    void SwitchState(bool isUnderwater)
    {
        if (isUnderwater)
        {
            rigid.drag = underWaterDrag;
            rigid.angularDrag = underWaterAngularDrag;
        }
        else
        {
            rigid.drag = airDrag;
            rigid.angularDrag = airAngularDrag;
        }
    }
}
