using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PremikanjeRibe : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 Cilj;
    private Vector3 izhodisce;
    private Vector3 prejsnjiCilj;
    public float hitrost=0.05f;
    public bool naCilju=false;
    

    void Start()
    {
        izhodisce = transform.position;
            
    }

    // Update is called once per frame
    void Update()
    {
        float korak = hitrost * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position,Cilj, korak);

        if (Vector3.Distance(Cilj, transform.position) < 0.01f)
        {
            if (naCilju == false)
            {
                naCilju = true;
                prejsnjiCilj = Cilj;
                Cilj = izhodisce;
            }
            else
            {
                naCilju = false;
                Cilj = prejsnjiCilj;

            }
            


        }
    }
}
