#include <RotaryEncoder.h>

#define PIN_IN1 8
#define PIN_IN2 9

#define SW 7
#define DT 8
#define CLK 9
#define GUMB 3
//problem 7, 0, 2
//dela 8, 5, 12, 13, 14
int pot;
int stevecObratov = 0;


bool strel = 0;
RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::TWO03);
void setup() {
  Serial.begin(9600);
  //Serial.println("blabla");


  pinMode(GUMB, INPUT_PULLUP);
  //Serial.println("blabla GUMB");

}

void loop() {
  static int pos = 0;
  encoder.tick();
  // put your main code here, to run repeatedly:
  pot = analogRead(A0);
  strel = digitalRead(GUMB);
  //Serial.println(strel);
  //Serial.println(pot);

  int smerObracanja = (int)encoder.getDirection();
  int vrednostObracanja = (int)encoder.getPosition();
  Serial.print(pot);
  Serial.print(',');
  Serial.print(strel);
  Serial.print(',');
  Serial.print(smerObracanja);
  Serial.print(',');
  Serial.println(vrednostObracanja);
  delay(1);
  Serial.flush();


}
